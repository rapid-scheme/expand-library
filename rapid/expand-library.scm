;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (expand-library library-name exports imports body)
  (parameterize ((current-syntactic-environment
		  (imports->syntactic-environment imports)))
    (let* ((definitions
	     (append-map-in-order expand-top-level-form body))
	   (definitions
	     (map (lambda (definition)
		    (syntax-match definition
		      ((define-values ,formals ,expression)	       
		       (syntax (define-values ,formals
				 ,(force-syntax expression))))))
		  definitions)))
      (mapping-for-each
       (lambda (identifier qid)
	 (when (and (equal? (qualified-identifier-library-name qid)
			    library-name)
		    (not (lookup-binding!/default identifier #f)))
	   (raise-syntax-error (qualified-identifier-context qid)
			       "identifier ‘~a’ exported, but not bound"
			       identifier)))
       exports)
      (values (current-syntactic-environment) definitions))))

(define (library-environment library-name)
  ((library-environment-accessor) library-name))

(define (imports->syntactic-environment imports)
  (parameterize ((current-syntactic-environment
		  (make-syntactic-environment)))
    (mapping-for-each
     (lambda (identifier qualified-identifier)
       (let ((library-name (qualified-identifier-library-name qualified-identifier))
	     (symbol (qualified-identifier-symbol qualified-identifier)))
	 (let ((library-environment (library-environment library-name)))
	   (and-let* ((binding
		       (parameterize ((current-syntactic-environment
				       library-environment))
			 (lookup-binding!/default symbol #f)))
		      (denotation (binding-denotation binding)))
	     (insert-binding! identifier denotation #t)))))
     imports)
    (current-syntactic-environment)))

(define (expand-top-level-form form)
  (maybe-isolate
   #t
   (lambda ()
     (syntax-match (macro-expand form)
       ((begin ,form* ...)
	(append-map-in-order expand-top-level-form form*))
       ((define-syntax ,name ,transformer)
	(guard (identifier? (unwrap-syntax name)))
	(cond
	 ((expand-transformer transformer)
	  => (lambda (transformer)
	       (insert-binding! name (make-denotation transformer #f))))))
       ((define-syntax . ,_)
	(raise-syntax-error form
			    "invalid define-syntax syntax")
	'())
       ((define-values (,arg1* ... . ,arg2*) ,expression)
	(or
	 (and (invalid-formals? `(,arg1* ... . ,arg2*)) '())
	 (let ((loc1* (map-in-order generate-top-level-location! arg1*))
	       (loc2* (map-in-order* generate-top-level-location! arg2*)))  
	   `(,(syntax (define-values (,loc1* ... . ,loc2*)
			,(lambda () (expand-expression expression))))
	     ,@(fold-right (lambda (arg loc acc)
			     (let* ((binding (lookup-binding! arg))
				    (denotation (binding-denotation binding))
				    (location (denotation-location denotation)))
			       (if (eq? location loc)
				   acc
				   `(,(syntax (define-values ,(gensym 'dummy)
						(set! ,location ,loc))) . ,acc))))
			      '() (append-reverse* arg2* arg1*)
			      (append-reverse* loc2* loc1*))))))
       ((define-values . ,_)
	(raise-syntax-error form
			    "invalid define-values syntax")
	'())
       (,expression
	(list (syntax (define-values ,(gensym 'dummy)
			,(expand-expression/expanded expression)))))))))

(define (expand-body context body)
  (parameterize ((current-syntactic-environment
		  (new-syntactic-environment)))
    (let loop ((body body) (bindings '()))
      (cond
       ((null? body)
	(raise-syntax-error context "no expression in body")
	(syntax (if #f #f)))
       (else
	(let ((form (car body)))
	  (syntax-match (macro-expand form)
	    ((begin ,form* ...)
	     (loop (append form* (cdr body)) bindings))
	    ((define-values (,arg1* ... . ,arg2*) ,expression)
	     (or
	      (and (invalid-formals? `(,arg1* ... . arg2*)) '())
	      (let ((loc1* (map-in-order generate-location! arg1*))
		    (loc2* (map-in-order* generate-location! arg2*)))
		(loop (cdr body)
		      (cons (syntax ((,loc1* ... . ,loc2*)
				     ,(lambda ()
					(expand-expression expression))))
			    bindings)))))
	    ((define-values . ,_)
	     (raise-syntax-error form "invalid define-values syntax")
	     (loop (cdr body) bindings))
	    ((define-syntax ,name ,transformer)
	     (guard (identifier? (unwrap-syntax name)))
	     (cond
	      ((expand-transformer transformer)
	       => (lambda (transformer)
		    (insert-binding! name (make-denotation transformer #f)))))
	     (loop (cdr body) bindings))
	    ((define-syntax . ,_)
	     (raise-syntax-error form "invalid define-syntax syntax")
	     (loop (cdr body) bindings))
	    (,expression
	     (let ((bindings
		    (map (lambda (binding)
			   (syntax-match binding
			     ((,formals ,expression)
			      (syntax (,formals ,(force-syntax expression))))))
			 (reverse bindings))))
	       (let ((expanded-expression (expand-expression/expanded expression)))
		 (syntax (letrec*-values ,(reverse bindings)
					 (begin
					   ,@(cons expanded-expression
						   (map-in-order expand-expression
								 (cdr body))))))))))))))))

(define (expand-expression form)
  (expand-expression/expanded (macro-expand form)))

(define (expand-expression/expanded form)
  (syntax-match form
    ;; Sequence
    ((begin ,(expand-expression -> form*) ...)
     (syntax (begin ,form* ...)))

    ;; Definition
    ((define-values . ,_)
     (raise-syntax-error form "unexpected definition")
     (syntax (if #f #f)))

    ;; Syntax definition
    ((define-syntax . ,_)
     (raise-syntax-error form "unexpected syntax definition")
     (syntax (if #f #f)))
    
    ;; Syntax error
    ((syntax-error ,message . ,args)
     (guard (string? (unwrap-syntax message)))
     (if (null? args)
	 (raise-syntax-error form (unwrap-syntax message))
	 (raise-syntax-error form
			     "~a: ~a"
			     (unwrap-syntax message)
			     (map strip-syntax args)))
     (syntax (if #f #f)))
    ((syntax-error . ,_)
     (raise-syntax-error form "invalid syntax-error syntax")
     (syntax (if #f #f)))

    ;; Conditional
    ((if ,(expand-expression -> test) ,(expand-expression -> consequent))
     (syntax (if ,test ,consequent (if #f #f))))
    ((if ,(expand-expression -> test)
	 ,(expand-expression -> consequent)
	 ,(expand-expression -> alternate))
     (syntax (if ,test ,consequent ,alternate)))
    ((if . ,_)
     (raise-syntax-error form "invalid if syntax")
     (syntax (if #f #f)))

    ;; Assignment
    ((set! ,variable ,(expand-expression -> expression))
     (guard (identifier? (unwrap-syntax variable)))
     (let ((location (denotation-location (lookup! variable))))
       (cond
	(location
	 (syntax (set! ,location ,expression)))
	(else
	 (raise-syntax-error form
			     "cannot assign a value to syntax ‘~a’"
			     (identifier->symbol variable))
	 (syntax (if #f #f))))))
    ((set! . ,_)
     (raise-syntax-error form "invalid set! syntax")
     (syntax (if #f #f)))
    
    ;; Quotation
    ((quote ,datum)
     (syntax (quote ,(strip-syntax datum))))
    ((quote . ,_)
     (raise-syntax-error form "invalid quote syntax")
     (syntax (if #f #f)))

    ;; Abstraction
    ((lambda (,arg1* ... . ,arg2*)
       ,body* ...)
     (or
      (and (invalid-formals? `(,arg1* ... . ,arg2*))
	   (syntax (if #f #f)))
      (parameterize ((current-syntactic-environment
		      (new-syntactic-environment)))
	(let ((loc1* (map-in-order generate-location! arg1*))
	      (loc2* (map-in-order* generate-location! arg2*)))
	  (syntax (lambda (,loc1* ... . ,loc2*)
		    ,(expand-body form body*)))))))
    ((lambda . ,_)
     (raise-syntax-error form
			 "invalid lambda syntax")
     (syntax (if #f #f)))

    ;; Cond-expand
    ((cond-expand ,clause1 ,clause2* ...)
     (let ((form* (expand-cond-expand (cons clause1 clause2*))))
       (if (null? form*)
	   (syntax (if #f #f))
	   ;; This may be a problem when "begin" has to be looked up
	   (syntax (begin ,@(map expand-expression form*))))))
    ((cond-expand . ,_)
     (raise-syntax-error form "invalid cond-expand syntax")
     (syntax (if #f #f)))

    ;; Inclusion
    ((include ,string1 ,string2* ...)
     (guard (every (lambda (syntax) (string? (unwrap-syntax syntax))) (cons string1 string2*)))
     (expand-include form (cons string1 string2*) #f))
    ((include . ,_)
     (raise-syntax-error form "invalid include syntax"))
    ((include-ci ,string1 ,string2* ...)
     (guard (every (lambda (syntax) (string? (unwrap-syntax syntax))) (cons string1 string2*)))
     (expand-include form (cons string1 string2*) #t))
    ((include-ci . ,_)
     (raise-syntax-error form "invalid include-ci syntax"))
    
    ;; Auxiliary syntax
    ((,keyword . ,_)
     (guard (memq (unwrap-syntax keyword) '(... _))) 
     (raise-syntax-error form
			 "invalid use of auxiliary syntax ‘~a’"
			 (unwrap-syntax keyword)))
    
    ;; Primitive operators
    ((,operator ,(expand-expression -> operand*) ...)
     (syntax (,operator ,operand* ...)))

    ;; Variable reference
    (,expression
     (guard (identifier? (unwrap-syntax expression)))
     (let ((location (denotation-location (lookup! expression))))
       (or location
	   (begin
	     (raise-syntax-error form
				 "invalid use of syntax ‘~a’ as value"
				 (identifier->symbol (unwrap-syntax expression)))
	     (syntax (if #f #f))))))
    
    ;; Literal
    (,expression
     (guard (literal? (unwrap-syntax expression)))
     (syntax (quote ,(strip-syntax expression))))

    ;; Invalid expression
    (,expr
     (raise-syntax-error expr "invalid expression")
     (syntax (if #f #f)))))

(define (expand-transformer transformer)
  (syntax-match transformer
    ((,(lookup! -> keyword) ,datum* ...)
     (case (and (denotation? keyword) (denotation-transformer keyword))
       ((syntax-rules)
	(expand-syntax-rules-transformer transformer))
       (else
	(raise-syntax-error transformer "invalid macro transformer"))))
    (,form
     (raise-syntax-error transformer "macro transformer expected"))))

(define (expand-syntax-rules-transformer transformer)
  (call/cc
   (lambda (return)
     (receive (ellipsis* literal* syntax-rule*)
	 (syntax-match transformer
	   ((,_ (,literal* ...) ,syntax-rule* ...)
	    (guard (every identifier-syntax? literal*))
	    (values '() literal* syntax-rule*))
	   ((,_ ,ellipsis (,literal* ...) ,syntax-rule* ...)
	    (guard (every identifier-syntax? (cons ellipsis literal*)))
	    (values (list (unwrap-syntax ellipsis)) literal* syntax-rule*))
	   (,_
	    (raise-syntax-error transformer "invalid syntax-rules syntax")
	    (return #f)))
       (define literals (make-literal-mapping literal*))
       (define (literal? identifier)
	 (and (mapping-ref/default literals identifier #f)
	      (begin
		(syntactic-environment-ref identifier)
		#t)))
       (define ellipsis?
	 (if (null? ellipsis*)
	     (auxiliary-syntax-comparator '...)		 
	     (lambda (identifier)
	       (bound-identifier=? identifier (car ellipsis*)))))
       (define underscore? (auxiliary-syntax-comparator '_))
       (make-syntax-rules-transformer transformer
				      ellipsis?
				      literal?
				      underscore?
				      syntax-rule*)))))
  
(define (make-literal-mapping literals)
  (define free-identifier-comparator (make-free-identifier-comparator))
  (fold (lambda (literal mapping)
	  (mapping-update mapping
			  (unwrap-syntax literal)
			  values
			  (lambda ()
			    (syntactic-environment-ref (unwrap-syntax literal))
			    literal)
			  (lambda (previous-literal)
			    (raise-syntax-error literal
						"duplicate literal identifier ‘~a’"
						(identifier->symbol (unwrap-syntax literal)))
			    (raise-syntax-note previous-literal
					       "previous occurrence was here"))))
	(mapping free-identifier-comparator)
	literals))

(define (auxiliary-syntax-comparator symbol)
  (lambda (identifier)
    (and-let* (((not (literal? identifier)))
	       (binding (syntactic-environment-ref (current-syntactic-environment)
						   identifier))
	       (denotation (binding-denotation binding))
	       (transformer (denotation-transformer denotation))
	       (eq? transformer symbol))
      (syntactic-environment-ref identifier)
      #t)))

(define (macro-expand form)
  (syntax-match form
    ((,(lookup! -> keyword) ,datum* ...)
     (cond
      ((not (denotation? keyword))
       (syntax (apply ,keyword ,datum* ... '())))
      ((and (denotation? keyword)
	    (denotation-transformer keyword))
       => (lambda (transformer)
	    (cond
	     ((symbol? transformer)
	      (syntax (,transformer ,datum* ...)))
	     (else
	      (macro-expand (transformer form))))))
      (else
       (syntax (,(denotation-location keyword) ,datum* ...)))))
    ((,datum1* ,datum2* ... . ,datum3*)
     (syntax (syntax-error "dotted list in source")))
    (,form
     (if (circular-list? form)
	 (syntax (syntax-error "circular list in source"))
	 form))))

(define (expand-include context filename* ci?)
  (let ((expression* (reverse (read-files-reverse context
						  filename*
						  '()
						  ci?))))
    (cond
     ((null? expression*)
      (raise-syntax-error context "no expressions read")
      (syntax (if #f #f)))
     (else
      (syntax (begin ,@(map expand-expression expression*)))))))

(define (generate-top-level-location! identifier-syntax)
  (let ((identifier (unwrap-syntax identifier-syntax)))
    (let ((location (generate-location identifier)))
      (or
       (and-let*
	   ((binding (lookup-binding!/default identifier-syntax #f))
	    ((location? (binding-denotation binding))))
	 binding)
       (begin
	 (insert-binding! identifier-syntax (make-denotation #f location))
	 location)))))

(define (generate-location! identifier-syntax)
  (let ((identifier (unwrap-syntax identifier-syntax)))
    (let ((location (generate-location identifier)))
      (insert-binding! identifier-syntax (make-denotation #f location))
      location)))

(define (lookup! identifier-syntax)
  (let ((identifier (unwrap-syntax identifier-syntax))
	#;
	(globals (get-globals)))
    (cond
     ((not (identifier? identifier))
      identifier-syntax)
     #;
     ((and globals
	   (mapping-ref/default globals identifier #f))
      => global-denotation)
     ((lookup-binding!/default identifier-syntax #f)
      => binding-denotation)
     (else
      (raise-syntax-error identifier-syntax
			  "identifier ‘~a’ referenced, but not bound"
			  (identifier->symbol identifier))
      (make-denotation #f (gensym 'undefined)))
     #;
     (else
      (let ((denotation (make-denotation #f (generate-location identifier))))
	(set-globals! (mapping-set globals identifier
				   (make-global identifier-syntax denotation)))
	denotation)))))

(define (insert! identifier-syntax denotation)
  (let ((identifier (unwrap-syntax identifier-syntax)))
    #;
    (set-globals! (mapping-delete (get-globals) identifier))
    (insert-binding! identifier-syntax denotation)))

(define (lookup-location! syntax)
  (and-let* ((datum (unwrap-syntax syntax))
	     ((identifier? datum)))
    (denotation-location (lookup! syntax))))

(define (generate-location identifier)
  (gensym (identifier->symbol identifier)))

;;;; Helper procedures

(define (force-syntax stx)
  (let ((datum (unwrap-syntax stx)))
    (if (procedure? datum)
	(syntax stx ,(datum))
	stx)))

(define (literal? datum)
  (or (boolean? datum) (vector? datum) (string? datum) (number? datum) (bytevector? datum)))

(define (append-map-in-order proc . clist*)
  (let ((list* (apply map-in-order proc clist*)))
    (concatenate list*)))

(define (strip datum)
  (if (identifier? datum)
      (identifier->symbol datum)
      datum))

(define (strip-syntax syntax)
  (syntax->datum syntax strip))

(define (map-in-order* proc flist)
  (let loop ((flist flist))
    (cond
     ((null? flist)
      '())
     ((pair? flist)
      (let ((head (proc (car flist))))
	(cons head (loop (cdr flist)))))
     (else
      (proc flist)))))

(define (append-reverse* head tail)
  (cond
   ((null? head)
    tail)
   ((pair? head)
    (append-reverse* (cdr head) (cons (car head) tail)))
   (else
    (cons head tail))))

(define (fold* proc acc list)
  (let loop ((list list) (acc acc))
    (cond
     ((pair? list)
      (loop (cdr list) (proc (car list) acc)))
     ((null? list)
      acc)
     (else
      (proc list acc)))))

(define (identifier-syntax? syntax)
  (identifier? (unwrap-syntax syntax)))

(define (invalid-formals? formals)
  (call-with-current-continuation
   (lambda (return)
     (fold* (lambda (formal formals)
	      (let ((datum (unwrap-syntax formal)))
		(unless (identifier? datum)
		  (raise-syntax-error formal
				      "identifier expected")
		  (return #t))
		(mapping-update! formals
				 datum
				 values
				 (lambda ()
				   formal)
				 (lambda (previous-formal)
				   (raise-syntax-error formal
						       "duplicate variable ‘~a’"
						       (identifier->symbol datum))
				   (raise-syntax-note previous-formal
						      "previous occurrence was here")
				   (return #t)))))
	    (mapping identifier-comparator) formals)
     #f)))

;;;; Parameter objects

(define library-environment-accessor
  (make-parameter #f))

(define (location? denotation)
  (and (denotation-location denotation) #t))

