;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> R7RS library expander.

(define-library (rapid expand-library)
  (export expand-library
	  expand-expression
	  imports->syntactic-environment
	  library-environment
	  library-environment-accessor)
  (import (except (scheme base) quasiquote)           (scheme write) ;;XXX
	  (scheme lazy)
	  (rapid receive)
	  (rapid and-let)
	  (rapid quasiquote)
	  (rapid box)
	  (rapid and-let)
	  (rapid list)
	  (rapid mapping)
	  (rapid syntax)
	  (rapid read)
	  (rapid gensym)
	  (rapid analyze-library)
	  (rapid syntactic-environment)
	  (rapid macro-transformer))
  (include "expand-library.scm"))
