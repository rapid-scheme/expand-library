;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid expand-library-test)
  (export run-tests)
  (import (scheme base)
	  (scheme file)
	  (scheme write)
	  (scheme eval)
	  (rapid test)
	  (rapid assume)
	  (rapid set)
	  (rapid mapping)
	  (rapid vicinity)
	  (rapid syntax)
	  (rapid primitives)
	  (rapid syntactic-environment)
	  (rapid analyze-library)
	  (rapid expand-library))
  (begin

    (define (test-library-exports context library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	rapid-primitive-exports)
       (else
	(assume #f))))

    (define (test-library-dependencies library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	(set library-name-comparator))
       (else
	(assume #f))))
    
    (define (test-library-environment library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	rapid-primitive-environment)
       (else
	(assume #f))))

    (define (test-feature-identifier? feature)
      (and (memq feature '(r7rs rapid-scheme)) #t))

    (define-syntax with-test-environment
      (syntax-rules ()
	((with-test-environment thunk)
	 (parameterize
	    ((library-environment-accessor test-library-environment)
	     (library-exports-accessor test-library-exports)
	     (library-dependencies-accessor test-library-dependencies)
	     (feature-identifier-predicate test-feature-identifier?))
	   (thunk)))))	    

    (define test-env (environment '(rename (scheme base)
					   (+ fx+))))
    (define (test-eval expr)
      (eval `(let-syntax
		 ((letrec*-values
		   (syntax-rules ()
		     ((letrec*-values ((formals init) ...) body)
		      (let ()
			(define-values formals init) ...
			body)))))
	       ,(syntax->datum expr))
	       test-env))
    
    (define-syntax test-expression
      (syntax-rules ()
	((test-expression name expr)
	 (let ((%expr (syntax ,expr)))
	   (test-equal name
	     (test-eval %expr)
	     (test-eval
	      (parameterize ((feature-identifier-predicate
			      test-feature-identifier?)
			     (current-syntactic-environment
			      rapid-primitive-environment))
		(guard (condition
			((syntax-error-object? condition)
			 (syntax-error-object->string condition)))
		  (expand-expression (syntax ,expr)))
		(expand-expression %expr))))))))

    (define-syntax test-syntax-error
      (syntax-rules ()
	((test-syntax-error message expr)
	 (test-equal
	     message
	     (parameterize ((feature-identifier-predicate
			     test-feature-identifier?)
			    (current-syntactic-environment
			     rapid-primitive-environment))
	     (guard (condition
		     ((syntax-error-object? condition)
		      (syntax-error-object->string condition)))
	       (expand-expression (syntax ,expr))))))))

    (define (write-data!)
      (define data
	'((fx+ x X)))
      (define file-name (in-vicinity (user-vicinity) "data.scm"))
      (define (write-file!)
	(unless (file-exists? file-name)
	  (with-output-to-file file-name
	    (lambda ()
	      (for-each (lambda (datum)
			  (write datum)
			  (newline)) data)))))
      (write-file!))
    
    (define (run-tests)
      (write-data!)
      
      (test-begin "Library expander")

      (test-group "expand-expression"
	
	(test-expression "Operators and literals"
	  '(fx+ 1 2))
	(test-expression "Abstraction and application with no argument"
	  '((lambda () 1)))
	(test-expression "Abstractions, applications, and variable references"
	  '((lambda (x) (fx+ x 1)) 1))
	(test-expression "Sequence and Assignment"
	  '((lambda (x) (begin (set! x 2) x)) 1))
	(test-expression "Conditional and Quotation"
	  '(if #t 'a 'b))
	(test-expression "Cond-expand"
	  '(cond-expand (r8rs #f) (r7rs #t)))
	(test-expression "include"
	  '((lambda (x X) (include "data.scm")) 1 2))
	(test-expression "include"
	  '((lambda (x X) (include-ci "data.scm")) 1 2))
	(test-expression "procedures"
	  '((lambda (+)
	      (+ 10 20))
	    (lambda (a b) (fx+ a b))))
	(test-expression "local definitions"
	  '((lambda (bar)
	      (define-values (foo) (lambda () bar))
	      (define-values (bar) 2)
	      (foo))
	    1))

	(test-expression "define-syntax 1"
	  '((lambda ()
	      (define-syntax foo
		(syntax-rules ()
		  ((foo) 'foo)))
	      (foo))))
	(test-expression "define-syntax 2"
	  '((lambda ()
	      (define-syntax foo
		(syntax-rules (bar)
		  ((foo bar baz) '(foo baz))))
	      (foo bar quux))))

	(test-syntax-error "error: duplicate variable ‘x’"
	  '(lambda (x y . x) 1))	
	(test-syntax-error "error: syntax-error: (foo)"
	  '(syntax-error "syntax-error" foo))
	(test-syntax-error "error: invalid use of auxiliary syntax ‘...’"
	  '(...))
	(test-syntax-error "error: invalid use of syntax ‘...’ as value"
	  '...)
	(test-syntax-error "error: invalid expression"
	  '(syntax-rules ()))
	(test-syntax-error "error: dotted list in source"
	  '(a . 2))
	(test-syntax-error "error: invalid macro transformer"
	  '(lambda (r)
	     (define-syntax foo
	       (lambda (e) e))
	     'bla))
	(test-syntax-error "error: no pattern variable to repeat here"
	  '((lambda ()
	      (define-syntax foo
		(syntax-rules (bar)
		  ((foo bar baz) '(foo baz ...))))
	      (foo bar quux)))))

      (test-group "expand-library"
	(with-test-environment
	 (lambda ()
	   (define-values (exports imports dependencies body)
	     (analyze-library
	      '(rapid)
	      (unwrap-syntax
	       (syntax
		((import (rapid primitive))
		 (begin
		   (define-values (a b) 10)
		   (define-values (a c) 11)
		   (define-values e 12)
		   (begin a
			  b)))))))

	    (define-values (environment definitions)
	      (expand-library '(rapid) exports imports body))

	    #f
	    ;; XXX
	    #;
	    (test-assert
		#;
		'((define-values dummy.0 foo)
		  (define-values dummy.1 bar)
		  (define-values (a.2 b.3) 10)
		  (define-values (a.4 c.5) 11)
		  (set! a.2 a.4))
	    (map syntax->datum definitions))
    
	  )))
      
      (test-end))

    ))

;; Local Variables:
;; eval: (put 'test-expression 'scheme-indent-function 1)
;; eval: (put 'test-syntax-error 'scheme-indent-function 1)
;; End:
